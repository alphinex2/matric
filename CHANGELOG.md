# Changelog
## 0.2.0
* Added Export function
* Fixed Menus in project view
* Fixed Buttons in project view
* Disabled "dimmable" option on project creation
* Disabled "monochrome" option on project creation

## 0.1.1
* Issue #1: Saving a project adds always ".json" to filename

## 0.1.0
* Inital Commit
>>>>>>> dev-export
