const electron = require("electron");
const settings = require("electron-settings");

$(document).ready(function() {
	// Event Listeners
	$(".settings").change(function() {
		var setting = $(this).data("s");
		var value = $(this).val();

		settings.set(setting, value);
	});

	$(".nav").click(function() {
		var nav = new Navigation();
		nav.openView($(this).data("nav"));
	});

	$(".settings").each(function() {
		if(settings.has($(this).data("s")))
		{
			$(this).val(settings.get($(this).data("s")));
		}
	})
});

class Navigation
{
	constructor()
	{
		this.bw = require("electron").remote.getCurrentWindow();
	}

	openView(viewname)
	{
		this.bw.loadFile("view/" + viewname + ".html");
	}

	newWindow(title, viewname)
	{
		var nrbw = require('electron').remote.BrowserWindow;
		var bw = new nrbw({
			'parent': "top",
			'modal': true,
			'width': 800,
			'height': 500,
			'title': title
		});

		bw.loadFile("view/" + viewname + ".html");
	}
}

Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

function getAppVersion()
{
	return "0.0.0a001";
}