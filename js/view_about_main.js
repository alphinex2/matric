class ViewAboutMain
{
	constructor()
	{
		this.refreshFromExternal();
	}

	refreshFromExternal()
	{
		$.ajax("../CONTRIBUTERS.txt", {async: true, success: function(data)  {
			console.log(data);

			$("#about_contributers").html(data);
		}, error: function(a, b, c) {
			console.log("Failed to load external ressource:");
			console.log(a);
			console.log(b);
			console.log(c);
		}});

		$.ajax("../LIBS.txt", {async: true, success: function(data)  {
			console.log(data);

			$("#about_libs").html(data);
		}, error: function(a, b, c) {
			console.log("Failed to load external ressource:");
			console.log(a);
			console.log(b);
			console.log(c);
		}});

		$.ajax("../LICENSE.txt", {async: true, success: function(data)  {
			console.log(data);

			$("#about_license").html(data);
		}, error: function(a, b, c) {
			console.log("Failed to load external ressource:");
			console.log(a);
			console.log(b);
			console.log(c);
		}});

		$.ajax("../package.json", {async: true, success: function(data)  {
			console.log(data);

			data = JSON.parse(data);

			$("#APPVERSION").html(data.version);
		}, error: function(a, b, c) {
			console.log("Failed to load external ressource:");
			console.log(a);
			console.log(b);
			console.log(c);
		}});
	}
}

new ViewAboutMain();