class ViewProjectMain
{
	constructor()
	{
		this.prj = new Project();
		this.prj.loadFromSettings();
		this.selectedLeds = [];
		this.selectedFrame = null;
		this.selectedSection = null;
		this.tmp = {
			'copy': {
				'color': null
			}
		};

		this.reloadWindowSettings();

		$(document).ready(function() {
			viewProjectMain.renderFramelist();

			$("#doExport").click(() => {
				viewProjectMain.navToExport();
			});

			$("#doSave").click(() => {
				viewProjectMain.saveProject();
			});
		});
	}

	reloadWindowSettings()
	{
		var bw = require("electron").remote.getCurrentWindow();
		bw.setTitle("Matric - \"" + this.prj.prjd.name + "\"");
	}

	refreshLedSelection()
	{
		console.log("Refreshing LED selection...");

		/*
			var row = $(this).data("row");
			var ledif = $(this).data("ledid");
			var led = $(this).data("led");
		*/

		var selectedLedsData = [];
		var selectedLeds = [];
		viewProjectMain.selectedLeds = [];

		$(".board_ledc:checked").each(function() {
			$(this).css({'border': '1px solid #0ae;'});

			selectedLeds.push($(this));
		});

		for(var i in selectedLeds)
		{
			selectedLedsData.push({
				'dom': {
					'led': selectedLeds[i].dom
				},
				'data': this.prj.getLedById(viewProjectMain.selectedFrame, selectedLeds[i][0].dataset.ledid)
			});

			viewProjectMain.selectedLeds.push(selectedLeds[i][0].dataset.ledid);
		}

		//console.log(selectedLeds);
		//console.log(selectedLedsData);

		this.renderColorChooser();
	}

	renderColorChooser(color)
	{
		if(this.selectedLeds.length == 0)
		{
			$("#wrappercolorchooser").html("");

			return false;
		}

		var colorMode = this.prj.prjd.pcb.color;
		var html = "";

		if(colorMode == "monochrome")
		{
			html += "<div class='colorinputbutton' data-color='1' style='background: #000;'></div>";
			html += "<div class='colorinputbutton' data-color='0' style='background: #FFF;'></div>";
		}
		else if(colorMode == "24")
		{
			var selectedColors = [];
			var averageColor = [];
			var led = null;
			var tmp = 0;

			for(var selectedLed in this.selectedLeds)
			{
				led = this.prj.getLedById(this.selectedFrame, this.selectedLeds[selectedLed]);
				selectedColors.push(led.color);
			}

			for(var i = 0; i < 3; i++)
			{
				tmp = 0;

				for(var j = 0; j < selectedColors.length; j++)
				{
					tmp += parseInt(selectedColors[j][i]);
				}

				averageColor.push(Math.round(tmp / selectedColors.length));
			}

			html += "<div id='colorchooserlive'>";
			html += "<div class='w50 inline p20'>";
			html += "<div id='colorchooserpreview'></div>";
			html += "</div>";

			html += "<div class='w50 inline p20'>";
			
			html += "<div class='mb5'>";
			html += "<label for='colorchooser_r'>" + t("Red") + "</label>";
			html += "<input type='number' min='0' max='255' id='colorchooser_r' class='colorchoosercolor' data-color='r' name='colorchooser_r' value='" + averageColor[0] + "' />"
			html += "</div>";

			html += "<div class='mb5'>";
			html += "<label for='colorchooser_g'>" + t("Green") + "</label>";
			html += "<input type='number' min='0' max='255' id='colorchooser_g' class='colorchoosercolor' data-color='g' name='colorchooser_g' value='" + averageColor[1] + "' />"
			html += "</div>";

			html += "<div class='mb5'>";
			html += "<label for='colorchooser_b'>" + t("Blue") + "</label>";
			html += "<input type='number' min='0' max='255' id='colorchooser_b' class='colorchoosercolor' data-color='b' name='colorchooser_b' value='" + averageColor[2] + "' />"
			html += "</div>";

			html += "</div>";
			html += "</div>";

			html += "<div id='colorchooserpalette' class='bggray'>";
			html += "</div>";

			html += "<div id='colorchoosercmd'>";
			html += "<button id='colorchooserDoColorsave' class='ebb'><i class='mi'>save</i><span>" + t("To Palette") + "</span></button>";
			html += "<button id='colorchooserDoClearpalette' class='ebb'><i class='mi'>clear</i><span>" + t("Clear Palette") + "</span></button>";
			html += "</div>";
		}

		$("#wrappercolorchooser").html(html);

		this.recalcColorchooserPreview(false);
		this.renderColorChooserPalette();

		$(".colorchoosercolor").on('keyup', function() {
			if(colorMode == "24")
			{
				if($(this).val() < 0 || $(this).val() > 255)
				{
					$(this).val("0");
				}
			}

			viewProjectMain.recalcColorchooserPreview(true);
			viewProjectMain.renderBoard(viewProjectMain.selectedSection, viewProjectMain.selectedFrame);
		});

		$("#colorchooserDoColorsave").click(function() {
			if(colorMode == "24")
			{
				var choosenRGB = {
					'r': $("#colorchooser_r").val(),
					'g': $("#colorchooser_g").val(),
					'b': $("#colorchooser_b").val()
				};

				if(viewProjectMain.prj.addColorToPalette(choosenRGB.r, choosenRGB.g, choosenRGB.b))
				{
					viewProjectMain.renderColorChooserPalette();
				}
			}
		});

		$("#colorchooserDoClearpalette").click(function() {
			viewProjectMain.prj.prjd.palette = [];
			viewProjectMain.prj.onChanged();
			viewProjectMain.renderColorChooserPalette();
		});
	}

	renderColorChooserPalette()
	{
		var html = "";

		for(var i = 0; i < this.prj.prjd.palette.length; i++)
		{
			html += "<div class='colorchooserpalettecolor' data-key='" + this.prj.prjd.palette[i].key + "' style='background: rgb(" + this.prj.prjd.palette[i].r + "," + this.prj.prjd.palette[i].g + "," + this.prj.prjd.palette[i].b + ")' />"
		}

		$("#colorchooserpalette").html(html);

		$(".colorchooserpalettecolor").click(function() {
			console.log("clicked: .colorchooserpalettecolor");

			var item = viewProjectMain.prj.getPaletteColor($(this).data("key"));

			if(!item)
			{
				console.log("Could not find Color from palette. Rerender palette...");

				viewProjectMain.renderColorChooserPalette();

				return false;
			}

			$("#colorchooser_r").val(item.r == 0 ? "0" : item.r);
			$("#colorchooser_g").val(item.g == 0 ? "0" : item.g);
			$("#colorchooser_b").val(item.b == 0 ? "0" : item.b);

			viewProjectMain.recalcColorchooserPreview(true);
			viewProjectMain.renderBoard(viewProjectMain.selectedSection, viewProjectMain.selectedFrame);
		});
	}

	recalcColorchooserPreview(saveAtLeds)
	{
		var colorMode = this.prj.prjd.pcb.color;

		if(colorMode == "24")
		{
			var choosenRGB = {
				'r': $("#colorchooser_r").val(),
				'g': $("#colorchooser_g").val(),
				'b': $("#colorchooser_b").val()
			};

			if(saveAtLeds)
			{
				for(var selectedLed in this.selectedLeds)
				{
					if(!this.prj.updateLedColor(this.selectedFrame, this.selectedLeds[selectedLed], choosenRGB.r, choosenRGB.g, choosenRGB.b))
					{
						console.log("Failed to set color to led '" + this.selectedLeds[selectedLed] + "'!");
					}
				}
			}

			$("#colorchooserpreview").css({'background': 'rgb(' + choosenRGB.r + ',' + choosenRGB.g + ',' + choosenRGB.b + ')'})
		}
	}

	renderFramelist()
	{
		var data = this.prj.getSections();
		var html = "";

		for(var i = 0; i < data.length; i++)
		{
			if(data.id == 0)
			{
				continue;
			}

			html += "<div data-sectionid='" + data[i].id + "' class='p10-5 w100 framelist_section_header bggray'>" + (i + 1) + ": " + data[i].name + "</div>";
			html += "<div class='framelist_section w100 hidden pl10' data-sectionid='" + data[i].id + "' id='framelist_section" + data[i].id + "'>";

			for(var j = 0; j < data[i].frames.length; j++)
			{
				html += "<div data-sectionid='" + data[i].id + "' data-frameid='" + data[i].frames[j].id + "' class='p10-5 w100 framelist_frame_header bggray'>" + (j + 1) + ": " +  data[i].frames[j].name + "</div>";
				/*
				html += "<div class='framelist_frame w100 hidden pl10' data-frameid='" + data[i].frames[j].id + "' id='framelist_section" + data[i].frames[j].id + "'>";

				for(var k = 0; k < data[i].frames[j].rows; k++)
				{
					html += "<div data-frame='" + data[i].frames[j].id + "' class='p10-5 w100 framelist_frame_header bggray'>" + data[i].frames[j].name + "</div>";
					html 
				}

				html += "<button data-sectionid='" + data[i].id + "' class='doCreateFrame ebb'><i class='mi'>add</i>" + t("Add Frame") + "</button>"
				*/
			}

			html += "<button data-sectionid='" + data[i].id + "' class='doCreateFrame ebb'><i class='mi'>add</i>" + t("Add Frame") + "</button>"
			html += "</div>";
		}

		html += "<button class='doCreateSection ebb'><i class='mi'>add</i>" + t("Add Section") + "</button>"

		$("#wrapper_framelist").html(html);

		$(".framelist_section_header").click(function() {
			var id = $(this).data("sectionid");

			console.log("clicked: .framelist_section_header #" + id);

			$("#framelist_section" + id).slideToggle(100);
		});

		$(".framelist_frame_header").click(function() {
			viewProjectMain.selectedSection = $(this).data("sectionid");
			viewProjectMain.selectedFrame = $(this).data("frameid");
			viewProjectMain.selectedLeds = [];

			viewProjectMain.renderBoard(viewProjectMain.selectedSection, viewProjectMain.selectedFrame);
		});

		$(".doCreateSection").click(function() {
			console.log("clicked: .doCreateSection #0");

			viewProjectMain.prj.createNewSection();
			viewProjectMain.renderFramelist();
		});

		$(".doCreateFrame").click(function() {
			console.log("clicked: .doCreateFrame #0");

			if(!viewProjectMain.prj.createNewFrame($(this).data("sectionid")))
			{
				console.log("Adding frame failed!");
			}

			viewProjectMain.renderFramelist();
		});
	}

	renderBoard(sectionid, frameid)
	{
		this.actSectionid = sectionid;
		this.actFrameid = frameid;

		var sections = this.prj.getSections();
		var frame = null;

		for(var i = 0; i < sections.length; i++)
		{
			if(sections[i] == 0)
			{
				continue;
			}

			if(sections[i].id == sectionid)
			{
				for(var j = 0; j < sections[i].frames.length; j++)
				{
					//console.log("Check ID " + sectionid + ":" + frameid + " => " + sections[i].id + ":" + sections[i].frames[j].id);

					if(sections[i].frames[j].id == frameid)
					{
						frame = sections[i].frames[j];

						break;
					}
				}
			}
		}

		if(frame == null)
		{
			console.log("Failed to find frame!");
			alert("An error occured.");
		}

		var html = "";
		var led = null;
		
		html += "<div class='board_col_wrapper'>";

		for (var k = 0; k < this.prj.prjd.pcb.ledy; k++)
		{
			html += "<div class='board_col_ccontainer bggray'>";
			html += "<input type='checkbox' value='1' class='board_colc board_colc_col" + k + "' id='board_colc" + k + "' data-colid='" + k + "' data-col='" + k + "' />"
			html += "</div>";
		}

		html += "</div>";

		for(var i = 0; i < this.prj.prjd.pcb.ledx; i++)
		{
			html += "<div class='board_row' id='board_row" + i + "' data-rowid='" + i + "'>";
			html += "<div class='board_row_ccontainer bggray'>";
			html += "<input type='checkbox' value='1' class='board_rowc board_rowc_row" + i + "' id='board_rowc" + i + "' data-rowid='" + i + "' data-row='" + i + "' />"
			html += "</div>";

			for(var j = 0; j < this.prj.prjd.pcb.ledy; j++)
			{
				led = frame.rows[i][j];

				html += "<div style='background: rgb(" + led.color[0] + "," + led.color[1] + "," + led.color[2] + ");' class='board_led board_led_row" + i + "' id='board_led" + led.id + "' data-ledid='" + led.id + "' data-row='" + i + "' data-led='" + (j+i) + "'>";
				html += "<input type='checkbox' value='1' " + (this.selectedLeds.indexOf(led.id) > -1 ? "checked='checked'" : "") + " class='board_ledc board_ledc_row" + i + "' id='board_ledc" + led.id + "' data-ledid='" + led.id + "' data-row='" + i + "' data-led='" + (j+i) + "' />"
				html += "</div>";
			}

			html += "</div>";
		}

		$("#wrapper_board").html(html);

		$(".board_led").click(function() {
			//$(".board_led").css({'border': '1px solid transparent'});
			viewProjectMain.refreshLedSelection();
		});
	}

	saveProject(force)
	{
		if(this.prj.prjd.meta.savepath == null || (typeof undefined != typeof force && force))
		{
			var elr = require("electron").remote;

			elr.dialog.showSaveDialog(elr.getCurrentWindow(), {
				'title': t("Save Project"),
				'buttonLabel': t("Save Project"),
			}, function(filename, bookmark) {
				var fnp = filename.split(".");
				if(fnp[fnp.length - 1] != "json")
				{
					filename = filename + ".json";
				}

				console.log("Saving project under '" + filename + "'...");

				viewProjectMain.prj.prjd.meta.savepath = filename;
				viewProjectMain.prj.onChanged();

				if(viewProjectMain.prj.saveProject(filename))
				{
					viewProjectMain.setStatusbarMessage(t("Project saved!"));
				}
			});
		}
		else
		{
			this.saveProjectDirectly(this.prj.prjd.meta.savepath);
		}
	}

	saveProjectDirectly(path)
	{
		this.prj.saveProject(path);
	}

	setStatusbarMessage(message, persistent)
	{
		$("#statusbar").html(message);
	
		if(!persistent || typeof undefined == typeof persistent)
		{
			setTimeout(function() {
				$("#statusbar").html("");
			}, 10000);
		}
	}

	navToExport()
	{
		this.prj.onChanged();
		
		var nav = new Navigation();
		nav.openView("export");
	}
}

var viewProjectMain = new ViewProjectMain();