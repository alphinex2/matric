class Project
{
	constructor()
	{
		this.fs = require('fs');
		this.prjd = {};
		this.ae = require("electron").remote;
		this.se = require("electron-settings");
	}

	createNew(name, ledx, ledy, color, dimmable)
	{
		this.prjd = {
			'name': name,
			'meta': {
				'version': '0.0.1'
			},
			'pcb': {
				'ledx': ledx,
				'ledy': ledy,
				'color': color,
				'dimmable': dimmable
			},
			'palette': [],
			'sections': []
		}

		this.createNewSection();
	}

	createNewSection()
	{
		var guid = this.guid();
		var sec = {
			'id': guid,
			'name': guid,
			'frames': []
		};

		this.prjd.sections.push(sec);
		this.createNewFrame(guid);

		return guid;
	}

	createNewFrame(sectionid)
	{
		console.log(sectionid);

		var guid = this.guid();
		var frm = {
			'id': guid,
			'name': guid,
			'rows': {}
		};

		var i = 0;
		var row = {};
		
		for(i = 0; i < this.prjd.pcb.ledx; i++)
		{
			row = {};

			for(var j = 0; j < this.prjd.pcb.ledy; j++)
			{
				var ledGuid = this.guid();

				row[j] = {
					'id': ledGuid,
					'pos': {
						'x': i,
						'y': j
					},
					'color': [0, 0, 0],
					'brightness': 9
				}
			}

			frm.rows[i] = row;
		}

		var foundAt = -1;

		for(i = 0; i < this.prjd.sections.length; i++)
		{
			if(this.prjd.sections[i].id == sectionid)
			{
				foundAt = i;
				break;
			}
		}

		if(foundAt == -1)
		{
			return false;
		}

		this.prjd.sections[foundAt].frames.push(frm);

		this.saveInSettings();

		return guid;
	}

	getSections()
	{
		//console.log(this.prjd);

		return this.prjd.sections;
	}

	getLedById(frameid, ledid)
	{
		//console.log("Project.getLedById('" + frameid + "', '" + ledid + "')");

		for(var i = 0; i < this.prjd.sections.length; i++)
		{
			for(var j = 0; j < this.prjd.sections[i].frames.length; j++)
			{
				if(this.prjd.sections[i].frames[j].id == frameid)
				{
					//console.log("Project.getLedById: Frame found!");

					for(var k = 0; k < Object.size(this.prjd.sections[i].frames[j].rows); k++)
					{
						for(var l = 0; l < Object.size(this.prjd.sections[i].frames[j].rows[k]); l++)
						{
							//console.log(l);
							//console.log(this.prjd.sections[i].frames[j].rows[k][l]);

							if(this.prjd.sections[i].frames[j].rows[k][l].id == ledid)
							{
								return this.prjd.sections[i].frames[j].rows[k][l];
								
								break;
							}
						}
					}
				}
			}
		}

		return false;
	}

	updateLedColor(frameid, ledid, r, g, b)
	{
		//console.log("Project.updateLedColor('" + frameid + "', '" + ledid + "', " + r + ", " + g + ", " + b + ")");

		for(var i = 0; i < this.prjd.sections.length; i++)
		{
			for(var j = 0; j < this.prjd.sections[i].frames.length; j++)
			{
				if(this.prjd.sections[i].frames[j].id == frameid)
				{
					//console.log("Project.getLedById: Frame found!");

					for(var k = 0; k < Object.size(this.prjd.sections[i].frames[j].rows); k++)
					{
						for(var l = 0; l < Object.size(this.prjd.sections[i].frames[j].rows[k]); l++)
						{
							//console.log(l);
							//console.log(this.prjd.sections[i].frames[j].rows[k][l]);

							if(this.prjd.sections[i].frames[j].rows[k][l].id == ledid)
							{
								this.prjd.sections[i].frames[j].rows[k][l].color = [r, g, b];

								this.onChanged();

								return true;
							}
						}
					}
				}
			}
		}

		return false;
	}

	addColorToPalette(r,g,b)
	{
		var key = r + ":" + g + ":" + b;

		for(var i = 0; i < this.prjd.palette.length; i++)
		{
			if(this.prjd.palette[i].key == key)
			{
				return false;
			}
		}

		this.prjd.palette.push({
			'key': key,
			'r': r,
			'g': g,
			'b': b
		});

		this.onChanged();

		return true;
	}

	getPaletteColor(key)
	{
		console.log("getPaletteColor('" + key + "')");

		for(var i = 0; i < this.prjd.palette.length; i++)
		{
			if(this.prjd.palette[i].key == key)
			{
				return this.prjd.palette[i];
			}
		}

		return false;
	}

	saveProject(path)
	{
		var toSaveData = JSON.parse(JSON.stringify(this.prjd));
		toSaveData.meta.savepath = null;

		this.fs.writeFile(path, JSON.stringify(toSaveData), (err) => {
			if (err) throw err;
			console.log('The file has been saved!');
		});

		return true;
	}

	loadProject(path, callback)
	{
		this.fs.readFile(path, (err, data) => {
			if (err) throw err;

			this.prjd = JSON.parse(data.toString('utf8'));

			callback();
		});
	}

	saveInSettings()
	{
		this.se.set("PRJ_ACTIVE", JSON.stringify(this.prjd));
	}

	loadFromSettings()
	{
		this.prjd = JSON.parse(this.se.get("PRJ_ACTIVE"));

		this.checkProject();
	}

	clearSettingsTemp()
	{
		this.se.delete("PRJ_ACTIVE");
	}

	checkProject()
	{
		var supportedVersions = {
			'0.0.0a001': [
				'0.0.1'
			]
		};

		if(typeof undefined == typeof this.prjd.meta || supportedVersions[getAppVersion()].indexOf(this.prjd.meta.version) == -1)
		{
			var bw = require("electron").remote;

			if(!(typeof undefined == typeof this.prjd.meta))
			{
				var txt = "Project version: '" + this.prjd.meta.version + "'";
				var compatibleVersions = [];
	
				for(var supportedVersionI in supportedVersions)
				{
					if(supportedVersions[supportedVersionI].indexOf(this.prjd.meta.version) != -1)
					{
						compatibleVersions.push(supportedVersionI);
					}
				}
	
				if(compatibleVersions.length > 0)
				{
					txt += "; Compatible software versions: ";
	
					for(var i = 0; i < compatibleVersions.length; i++)
					{
						txt += compatibleVersions + ","
					}
				}
			}

			bw.dialog.showErrorBox(t("Project Version not supported."), t("Your Project version is not compatible with the software version. Please use another Version."));

			var nav = new Navigation();
			nav.openView("start");
		}
	}

	onChanged()
	{
		this.saveInSettings();
	}

	guid()
	{
		function s4()
		{
		  return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
		}

		return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4();// + '-' + s4() + s4() + s4();
	}
}