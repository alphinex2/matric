const LOADED_LANGPACK = null;
LANGPACK = null;

function t(string)
{
	if(!settings.has("LANG") || settings.get("LANG") == "en")
	{
		return string;
	}
	else
	{
		// Load language package
		if(LOADED_LANGPACK === null || LOADED_LANGPACK != settings.get("LANG"))
		{
			$.getJSON({async: false, url: "../js/langpack/lang_" + settings.get("LANG") + ".json", success: function(data) {
				LANGPACK = data;
				LOADED_LANGPACK == settings.get("LANG");
			}});
		}

		// Translate!
		try
		{
			//console.log(string  + " => " + LANGPACK[string]);

			if(typeof LANGPACK[string] == typeof undefined)
			{
				throw "'" + string + "': Not found";
			}

			return LANGPACK[string];
		}
		catch(exp)
		{
			console.log("Error in Language pack (" + settings.get("LANG") + "): " + exp);

			return string;
		}
	}
}

function t_auto()
{
	$(".t").each(function() {
		if(typeof $(this).attr("value") !== typeof undefined && $(this).attr("value") !== false)
		{
			$(this).attr("value", t($(this).attr("value")));
		}
		else
		{
			$(this).html(	t(	$(this).html()	)	);
		}
	});
}

$(document).ready(function() {
	t_auto();
})