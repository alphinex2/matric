class DefaultMenu
{
	constructor()
	{
		var {Menu, MenuItem} = require("electron").remote;
		var menu = new Menu();
		var menuContent = [
			{
				'label': t("File"),
				'submenu': [
					{
						'label': t("Quit"),
						'role': 'close'
					},
					{
						'label': t("Startscreen"),
						'click': function(a,b,c) {
							var nav = new Navigation();
							nav.openView("start");
						}
					}
				]
			},
			{
				'label': t("Settings"),
				'submenu': [
					{
						'label': t("Reset"),
						'click': function(a,b,c) {
							settings.deleteAll();
							var nav = new Navigation();
							nav.openView("start");
						}
					},
					{
						'label': t("Toggle Developer Tools"),
						'click': function(a,b,c) {
							var we = require("electron").remote.getCurrentWindow();
							we.toggleDevTools();
						}
					},
					{
						'label': t("Reload all (Dangerous!)"),
						'click': function(a,b,c) {
							var we = require("electron").remote.getCurrentWindow();
							we.reload();
						}
					},
					{
						'label': t("About"),
						'click': function(a,b,c) {
							var nav = new Navigation();
							nav.newWindow(t("About"), "about");
						}
					}
				]
			}
		]
		menu.append(new MenuItem(menuContent[0]));
		menu.append(new MenuItem(menuContent[1]));
		
		Menu.setApplicationMenu(menu);
	}
}

new DefaultMenu();