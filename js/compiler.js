class ProjectCompiler
{
	constructor(prj)
	{
		this.prj = prj;
	}

	compile(speed, loop)
	{
		var rtn = this.compileHeader(speed, loop);
		rtn += this.compileAllSections();

		return rtn;
	}

	compileAllSections()
	{
		var rtn = "";
		var data = this.prj.getSections();

		for(var i = 0; i < data.length; i++)
		{
			if(data.id == 0)
			{
				continue;
			}

			rtn += "# Section: " + i + " - " + data[i].id + ": " + data[i].name + "\n";
			rtn += "SECTION\n";
			rtn += this.compileSection(data[i]);
		}

		return rtn;
	}

	compileSection(section)
	{
		var rtn = "";

		for(var i = 0; i < section.frames.length; i++)
		{
			rtn += "# Frame: " + i + " - " + section.frames[i].id + ": " + section.frames[i].name + "\n";
			rtn += "FRAME\n";

			for(var j in section.frames[i].rows)
			{
				for(var k in section.frames[i].rows[j])
				{
					console.log(i + "." + j + "." + k);
					
					rtn += section.frames[i].rows[j][k].color[0] + ";" + section.frames[i].rows[j][k].color[1] + ";" + section.frames[i].rows[j][k].color[2] + "|"
				}
			}
		}

		return rtn + "\n";
	}

	compileHeader(speed, loop)
	{
		speed = typeof undefined == typeof speed ? 10 : speed;
		loop = typeof undefined == typeof loop ? true : loop;

		var rtn = "";

		rtn += "SPEED=" + speed + ";"
		
		if(loop)
		{
			rtn += "LOOP;"
		}

		return rtn + "\n";
	}
}