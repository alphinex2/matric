class ViewStartMain
{
	constructor()
	{
		$(document).ready(function()
		{
			if(!settings.has("DONE_FS") || settings.get("DONE_FS") == '0')
			{
				var nav = new Navigation();
				nav.openView("setup");
			}

			if(settings.has("PRJ_ACTIVE"))
			{
				$("#openActiveProject").removeClass("hidden");
			}

			// Listener
			$("#openNewProject").click(function() {
				$("#newProject").slideToggle(200);
			});

			$("#openActiveProject").click(function() {
				var nav = new Navigation();
				nav.openView("project");
			});

			$("#openProjectfile").click(function() {
				var elr = require("electron").remote;

				elr.dialog.showOpenDialog(elr.getCurrentWindow(), {
					'title': t("Open Project"),
					'filters': {
						'extensions': ['json']
					}
				}, function(paths) {
					console.log("Loading project under '" + paths[0] + "'...");

					var pathE = paths[0].split(".");

					if(pathE[pathE.length - 1] != "json")
					{
						elr.dialog.showErrorBox(t("Unkown fileformat"), t("Projectfile has to be a .json file!"));

						$("#openProjectfile").click();

						return false;
					}

					var prj = new Project();

					prj.loadProject(paths[0], function() {
						prj.saveInSettings();

						var nav = new Navigation();
						nav.openView("project");
					});
				});
			});

			function createNewProject()
			{
				var values = {
					'name': $("#p_name").val(),
					'ledx': $("#p_pcb_ledx").val(),
					'ledy': $("#p_pcb_ledy").val(),
					'dimmable': $("#p_pcb_dimmable").val(),
					'color': $("#p_pcb_color").val()
				};
				
				console.log("Creating new Project with following values:");
				console.log(values);

				var prj = new Project();
				prj.createNew(values.name, values.ledx, values.ledy, values.color, values.dimmable);
				prj.saveInSettings();

				return prj;
			}
		
			function validateNewProject()
			{
				var errorbox = "";
		
				if($("#p_name").val() == "")
				{
					errorbox += "<div>" + t("Please choose a projectname.") + "</div>"
				}
		
				let val;
				val = $("#p_pcb_ledx").val();
		
				if(val == "" || parseInt(val) <= 0)
				{
					errorbox += "<div>" + t("Please sepcify the leds with at least 1.") + "</div>"
				}
		
				val = $("#p_pcb_ledy").val();
		
				if(val == "" || parseInt(val) <= 0)
				{
					errorbox += "<div>" + t("Please sepcify the leds with at least 1.") + "</div>"
				}

				if(errorbox != "")
				{
					$("#errors").html(errorbox).slideDown(200);
					return false;
				}
		
				return true;
			}

			$("#navToProject").click(function() {
				if(validateNewProject())
				{
					createNewProject();

					var nav = new Navigation();
					nav.openView("project");
				}
			});
		});
	}
}

new ViewStartMain();