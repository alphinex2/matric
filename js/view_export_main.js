class ViewExportMain
{
	constructor()
	{
		this.prj = new Project();
		this.prj.loadFromSettings();

		$(document).ready(function() {
			$("#doNavGoBack").click(function() {
				var nav = new Navigation();
				nav.openView("project");
			});

			$("#doExport").click(function() {
				viewExportMain.export();
			});
		});
	}

	export()
	{
		var speed = $("#e_speed").val();
		var loop = $("#e_loop").is(":checked");

		speed = speed == "" ? 10 : speed;

		var compiler = new ProjectCompiler(this.prj);
		var exportedText = compiler.compile(speed, loop);

		var elr = require("electron").remote;

		elr.dialog.showSaveDialog(elr.getCurrentWindow(), {
			'title': t("Export Project"),
			'buttonLabel': t("Export"),
		}, function(filename, bookmark) {
			filename = filename;

			console.log("Exporting project under '" + filename + "'...");

			var fs = require('fs');

			fs.writeFile(filename, exportedText, (err) => {
				if (err) throw err;
				console.log('The file has been saved!');
			});
		});

		return true;
	}
}

var viewExportMain = new ViewExportMain();