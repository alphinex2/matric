# Matric
## Program your LED Matrix!
This multi-platform software is for prgramming your LED Matrix. You can setup you matrix with all properties such as colors and size. The prgram is able to handle different sections and frames. For Details of the Features please have a look at the list further down.

### Implemented Features
* Sections
* Adding Sections
* Frames
* Adding Frames
* Programmable Board
* Color selection
* Color saving function per project
* Average used Color calculation per selected LEDs
* Save Project
* Open Project
* Export Project
* Project Version Check
* Multi-Language Support (Englisch; German)

### Planned Features (unsorted)
* Copy Frames
* Delete Frames
* Renaming Frames
* Move Frames
* Copy Sections
* Delete Sections
* Renaming Sections
* Move Sections
* Copy Pixels
* Move Pixels
* Automated Animation Generator
* Function to expand animations via Animation Packs
* Preview
* Loops
* LED Matrix Templates (Save / Loadable)
* Monochrome Color Mode (On / Off)
* Remember last state (open Section/Frame etc...)
* Application settings
* Frames as function Frames (e.g. loops / gotos)
* Optimized savelogic

## Help
## How to run the Project?
You will need a special software on your device to control the LED Matrix. This is the part you have eventually program by yourself. All you have to do is to control you LED Matrix with the values of the exported file from matric.

### Which boards are supported?
If the colormodes are the same your board does support (eventuelly the software on your controller can translalte it), matric does support every LED Matrix. You could also abuse the software to control something else, like your christmas tree :).

### Some Features has bugs or are missing
You can add an issue to this repository or contribute to the project.