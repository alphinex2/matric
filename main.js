const electron = require("electron");
const settings = require("electron-settings");
let window;

function createWindow() {
	window = new electron.BrowserWindow({
		'width': (settings.has("WIN_S_X") ? settings.get("WIN_S_X") : 800),
		'height':(settings.has("WIN_S_Y") ? settings.get("WIN_S_Y") : 500),
		'title': "Matric",
		'webPreferences': {
			'devTools': true,
			'webSecurity': false
		}
	});

	window.loadFile("view/start.html");

	window.on("close", () => {
		settings.set("WIN_S_X", this.width);
		settings.set("WIN_S_Y", this.height);
	})

	window.on('closed', () => {
		window = null;
	});
}

electron.app.on('ready', createWindow);

electron.app.on('window-all-closed', () => {
	if(process.platform !== 'darwin')
	{
		electron.app.quit();
	}
});

electron.app.on('activate', () => {
	if(window == null)
	{
		createWindow();
	}
});